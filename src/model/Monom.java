package model;

public class Monom implements Comparable <Monom>{
      private int coeficient;
      private int putere;
      
      public int getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(int coeficient) {
		this.coeficient = coeficient;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public Monom(int c,int p)
	{
		this.coeficient=c;
		this.putere=p;
	}

	@Override
	public int compareTo(Monom o) {
		if(this.putere>o.getPutere())
			return -1;
		else if(this.putere<o.getPutere())
			return 1;
      	return 0;
	}
}
