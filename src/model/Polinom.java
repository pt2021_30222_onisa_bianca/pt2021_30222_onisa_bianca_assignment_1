package model;
import java.util.ArrayList;

public class Polinom {
     private  ArrayList <Monom> polin;
        
     
     public Polinom()
     {
     	polin=new ArrayList<Monom>();
     }
     
     
        public ArrayList<Monom> getPolin() {
		return polin;
	}

	public void setPolin(ArrayList<Monom> polin) {
		this.polin = polin;
	}

}
