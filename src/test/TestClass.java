package test;

import controller.CalculePolinom;
import model.Monom;
import model.Polinom;

import java.util.Collections;

public class TestClass {
    private Polinom p1;
    private Polinom p2;
    private Polinom p3;
    //p1=x^2-4x+4
    //p2=x-2
    public TestClass()
    {
        p1=new Polinom();
        p2=new Polinom();
        p3=new Polinom();

        p1.getPolin().add(new Monom(1,2));
        p1.getPolin().add(new Monom(-4,1));
        p1.getPolin().add(new Monom(4,0));

        p2.getPolin().add(new Monom(1,1));
        p2.getPolin().add(new Monom(-2,0));


    }

    public Polinom getP1() {
        return p1;
    }

    public Polinom getP2() {
        return p2;
    }

    public Polinom getP3() {
        return p3;
    }

    //x^2-4x+4 + x-2 = x^2 - 3x + 2
    public boolean testAdunare(Polinom p1,Polinom p2)
    {
        Polinom p3= CalculePolinom.adunare(p1,p2);
        Polinom p4=new Polinom();
        p4.getPolin().add(new Monom(1,2));
        p4.getPolin().add(new Monom(-3,1));
        p4.getPolin().add(new Monom(2,0));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //x^2-4x+4 - (x-2) = x^2 - 5x + 6
    public boolean testScadere(Polinom p1,Polinom p2)
    {
        Polinom p3= CalculePolinom.scadere(p1,p2);
        Polinom p4=new Polinom();
        p4.getPolin().add(new Monom(1,2));
        p4.getPolin().add(new Monom(-5,1));
        p4.getPolin().add(new Monom(6,0));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //(x^2-4x+4) * (x-2) = x^3 - 6x^2 +12x - 8
    public boolean testInmultire(Polinom p1,Polinom p2)
    {
        Polinom p3= CalculePolinom.inmultire(p1,p2);
        Polinom p4=new Polinom();
        Collections.sort(p3.getPolin());
        p4.getPolin().add(new Monom(1,3));
        p4.getPolin().add(new Monom(-6,2));
        p4.getPolin().add(new Monom(12,1));
        p4.getPolin().add(new Monom(-8,0));
        p4.getPolin().add(new Monom(-8,5));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //(x^2-4x+4) * (x-2) = x-2
    public boolean testImpartireCat(Polinom p1,Polinom p2)
    {
        Polinom p3= CalculePolinom.impartire_cat(p1,p2);
        Polinom p4=new Polinom();
        Collections.sort(p3.getPolin());
        p4.getPolin().add(new Monom(1,1));
        p4.getPolin().add(new Monom(-2,0));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //(x^2-4x+4) * (x-2) = x-2   CU RESTUL 0
    public boolean testImpartireRest(Polinom p1,Polinom p2)
    {
        Polinom p3= CalculePolinom.impartire_rest(p1,p2);
        Polinom p4=new Polinom();
        Collections.sort(p3.getPolin());
        p4.getPolin().add(new Monom(0,0));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //(x^2-4x+4)' = 2x-4
    public boolean testDerivare(Polinom p1)
    {
        Polinom p3= CalculePolinom.derivare(p1);
        Polinom p4=new Polinom();
        Collections.sort(p3.getPolin());
        p4.getPolin().add(new Monom(2,1));
        p4.getPolin().add(new Monom(-4,0));
        p4.getPolin().add(new Monom(0,0));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }

    //integ(x^2-4x+4) = x^3 - 2x^2 + 4x
    public boolean testIntegrare(Polinom p1)
    {
        Polinom p3= CalculePolinom.integrare(p1);
        Polinom p4=new Polinom();
        Collections.sort(p3.getPolin());
        p4.getPolin().add(new Monom(1,3));
        p4.getPolin().add(new Monom(-2,2));
        p4.getPolin().add(new Monom(4,1));

        for(int i=0;i<p3.getPolin().size();i++)
        {
            if(p3.getPolin().get(i).getCoeficient()!=p4.getPolin().get(i).getCoeficient() || p4.getPolin().get(i).getPutere()!=p3.getPolin().get(i).getPutere())
                return false;
        }
        return true;
    }
}
