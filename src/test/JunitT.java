package test;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class JunitT {
    TestClass test;
    @Before
    public void setUp()
    {
        test=new TestClass();
    }
    @Test
    public void testAdunare()
    {
       assertTrue( test.testAdunare(test.getP1(),test.getP2()));
    }

    @Test
    public void testScadere()
    {
        assertTrue(test.testScadere(test.getP1(), test.getP2()));
    }

    @Test
    public void testInmultire()
    {
        assertTrue(test.testInmultire(test.getP1(), test.getP2()));
    }

    @Test
    public void testImpartireCat()
    {
        assertTrue(test.testImpartireCat(test.getP1(), test.getP2()));
    }

    @Test
    public void testImpartireRest()
    {
        assertTrue(test.testImpartireRest(test.getP1(), test.getP2()));
    }

    @Test
    public void testDerivare()
    {
        assertTrue(test.testDerivare(test.getP1()));
    }

    @Test
    public void testIntegrare()
    {
        assertTrue(test.testIntegrare(test.getP1()));
    }
}
