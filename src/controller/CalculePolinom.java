package controller;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import model.Monom;
import model.Polinom;

import static java.lang.Math.abs;

public class CalculePolinom {
	

    public static Polinom adunare(Polinom p1, Polinom p2)
    {
    	Polinom p3=new Polinom();
    	p3.setPolin(p1.getPolin());
    	for(Monom i: p3.getPolin())
    	{
    		for(Monom j: p2.getPolin())
    		{
    			if(i.getPutere()==j.getPutere())
    				{
    				 Monom m=CalculeMonom.aduna(i,j);
    				 for(int k=0;k<p3.getPolin().size();k++)
					 {
					 	if(p3.getPolin().get(k)==i)
						{
							p3.getPolin().set(k,m);
						}
					 }
    				}
    		}
    	}
    	boolean ok;
    	for (Monom i: p2.getPolin())
    	{
    		ok=false;
    		for(Monom j: p3.getPolin())
    		{
    			if(i.getPutere()==j.getPutere())
    				ok=true;
    		}
    		if(ok==false)
    			p3.getPolin().add(i);

    	}

    	return p3;
    }
    
    public static Polinom scadere(Polinom p1, Polinom p2)
    {
		Polinom p3=new Polinom();
		p3.setPolin(p1.getPolin());
		for(Monom i: p3.getPolin())
		{
			for(Monom j: p2.getPolin())
			{
				if(i.getPutere()==j.getPutere())
				{
					Monom m=CalculeMonom.scade(i,j);
					for(int k=0;k<p3.getPolin().size();k++)
					{
						if(p3.getPolin().get(k)==i)
						{
							p3.getPolin().set(k,m);
						}
					}
				}
			}
		}
		boolean ok;
		for (Monom i: p2.getPolin())
		{
			ok=false;
			for(Monom j: p3.getPolin())
			{
				if(i.getPutere()==j.getPutere())
					ok=true;
			}
			if(ok==false) {
				i.setCoeficient(-i.getCoeficient());
				p3.getPolin().add(i);
			}

		}

		return p3;
    }
    
    public static Polinom inmultire(Polinom p1, Polinom p2)
    {
    	Polinom p3=new Polinom();
    	
    	for(Monom i: p1.getPolin())
    	{
    		for(Monom j: p2.getPolin())
    		{
    			 Monom m=CalculeMonom.inmult(i,j);
    			 p3.getPolin().add(m);
    		}
    	}
    	for(int i=0;i<p3.getPolin().size();i++)
		{
			for(int j=i+1;j<p3.getPolin().size();j++)
				if(p3.getPolin().get(i).getPutere()==p3.getPolin().get(j).getPutere())
				{
					Monom m=CalculeMonom.aduna(p3.getPolin().get(i),p3.getPolin().get(j) );
					p3.getPolin().set(i,m);
					p3.getPolin().set(j,new Monom(0,0));
				}
		}



    	Iterator <Monom> it=p3.getPolin().iterator();
    	while(it.hasNext())
		{
			Monom m=it.next();
			if(m.getCoeficient()==0)
				it.remove();
		}

    	return p3;
    }
    
    public static Polinom integrare(Polinom p1)
    {
    	Polinom p2=new Polinom();
    	for(Monom i: p1.getPolin())
    	{
    		Monom m=CalculeMonom.integ(i);
    		p2.getPolin().add(m);
    	}
    	return p2;
    }
    
    public static Polinom derivare(Polinom p1)
    {
    	Polinom p2=new Polinom();
    	for(Monom i: p1.getPolin())
    	{
    		Monom m=CalculeMonom.deriv(i);
    		p2.getPolin().add(m);
    	}
    	return p2;
    }

    public static Polinom impartire_cat(Polinom p1, Polinom p2)
	{
		Collections.sort(p1.getPolin());
		Collections.sort(p2.getPolin());
		Polinom cat=new Polinom();
		Polinom copie=new Polinom();

		for(Monom i: p1.getPolin())
		{
			copie.getPolin().add(new Monom(i.getCoeficient(),i.getPutere()));
		}

		while(copie.getPolin().size() >0 && copie.getPolin().get(0).getPutere()>=p2.getPolin().get(0).getPutere())
		{
			Polinom descazut=new Polinom();
			Monom m=CalculeMonom.impart(copie.getPolin().get(0),p2.getPolin().get(0));
			cat.getPolin().add(m);
			Monom mm=new Monom(-m.getCoeficient(),m.getPutere());
			for(Monom i:p2.getPolin())
			{

				Monom aux=CalculeMonom.inmult(mm,i);
				descazut.getPolin().add(aux);
			}
			copie=CalculePolinom.adunare(copie,descazut);
			Iterator <Monom> it=copie.getPolin().iterator();
			while(it.hasNext())
			{
				Monom i=it.next();
				if(i.getCoeficient()==0)
					it.remove();
			}

				Collections.sort(copie.getPolin());
		}
		return cat;
	}

	public static Polinom impartire_rest(Polinom p1, Polinom p2)
	{
		Collections.sort(p1.getPolin());
		Collections.sort(p2.getPolin());
		Polinom cat=new Polinom();
		Polinom copie=new Polinom();

		for(Monom i: p1.getPolin())
		{
			copie.getPolin().add(new Monom(i.getCoeficient(),i.getPutere()));
		}

		while(copie.getPolin().size() >0 && copie.getPolin().get(0).getPutere()>=p2.getPolin().get(0).getPutere())
		{
			Polinom descazut=new Polinom();
			Monom m=CalculeMonom.impart(copie.getPolin().get(0),p2.getPolin().get(0));
			cat.getPolin().add(m);
			Monom mm=new Monom(-m.getCoeficient(),m.getPutere());
			for(Monom i:p2.getPolin())
			{

				Monom aux=CalculeMonom.inmult(mm,i);
				descazut.getPolin().add(aux);
			}
			copie=CalculePolinom.adunare(copie,descazut);
			Iterator <Monom> it=copie.getPolin().iterator();
			while(it.hasNext())
			{
				Monom i=it.next();
				if(i.getCoeficient()==0)
					it.remove();
			}

				Collections.sort(copie.getPolin());
		}
		return copie;
	}

    public static String conversie(Polinom p)
	{
	  String aux="";
	  Collections.sort(p.getPolin());

	  if(p.getPolin().size()==0)
	  	aux="0";
	  else {

		  for (Monom i : p.getPolin()) {
			  if (i.getCoeficient() == 0)
				  aux = aux + "";

			  else if (i.getCoeficient() == 1) {
				  if (i.getPutere() == 0) {
					  aux = aux + "1+";
				  } else if (i.getPutere() == 1) {
					  aux = aux + "x+";
				  } else if (i.getPutere() > 1) {
					  aux = aux + "x^" + String.valueOf(i.getPutere()) + "+";
				  }
			  }
			  else if (i.getCoeficient() == -1) {
				  if (i.getPutere() == 0) {
					  aux = aux + "-1+";
				  } else if (i.getPutere() == 1) {
					  aux = aux + "-x+";
				  } else if (i.getPutere() > 1) {
					  aux = aux + "-x^" + String.valueOf(i.getPutere()) + "+";
				  }
			  }
			  else if (i.getPutere() == 0) {
				  aux = aux + String.valueOf(i.getCoeficient()) + "+";
			  }
			  else if (i.getPutere() == 1) {
				  aux = aux + String.valueOf(i.getCoeficient()) + "x+";
			  }
			  else if (i.getPutere() > 1) {
				  aux = aux + String.valueOf(i.getCoeficient()) + "x^" + String.valueOf(i.getPutere()) + "+";
			  }
		  }
	  }
	  aux=aux.replace("+-","-");
	  aux=aux.replace("-+","-");
	  aux=aux.substring(0,aux.length()-1);
	  if(aux.equals("")) return "0";
	  return aux;
	}
}
