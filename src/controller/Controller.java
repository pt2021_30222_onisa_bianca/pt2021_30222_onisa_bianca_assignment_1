package controller;
import model.Polinom;
import view.View;
import model.Model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

	private static View v;
	private static Model m;

	public static View getV() {
		return v;
	}

	public void setV(View v) {
		this.v = v;
	}

	public static Model getM() {
		return m;
	}

	public void setM(Model m) {
		this.m = m;
	}

	public Controller() {
		v=new View();
		m=new Model();
		ExtragereDate.extrage();

		v.getFr().getAdunare().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getCatT().setText("");
					v.getFr().getRezT().setText("");
					m.setP3(CalculePolinom.adunare(m.getP1(), m.getP2()));
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
				} catch (Exception eq) {
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});

		v.getFr().getScadere().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getCatT().setText("");
					v.getFr().getRezT().setText("");
					m.setP3(CalculePolinom.scadere(m.getP1(), m.getP2()));
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
				}
				catch (Exception eq)
				{
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});

		v.getFr().getInmultire().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getCatT().setText("");
					v.getFr().getRezT().setText("");
					m.setP3(CalculePolinom.inmultire(m.getP1(), m.getP2()));
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
				} catch (Exception eq) {
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});

		v.getFr().getDerivare().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getCatT().setText("");
					v.getFr().getRezT().setText("");
					m.setP3(CalculePolinom.derivare(m.getP1()));
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
				} catch (Exception eq) {
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});

		v.getFr().getIntegrare().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getCatT().setText("");
					v.getFr().getRezT().setText("");
					m.setP3(CalculePolinom.integrare(m.getP1()));
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
				} catch (Exception eq) {
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});

		v.getFr().getImpartire().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					v.getFr().getRezT().setText("");
					v.getFr().getCatT().setText("");
					m.setP3(CalculePolinom.impartire_cat(m.getP1(),m.getP2()));
					Polinom rest=CalculePolinom.impartire_rest(m.getP1(),m.getP2());
					v.getFr().getRezT().setText(CalculePolinom.conversie(m.getP3()));
					v.getFr().getCatT().setText(CalculePolinom.conversie(rest));
				} catch (Exception eq) {
					Controller.getV().getE().getFrame1().setVisible(true);
				}
			}
		});
	}
}
