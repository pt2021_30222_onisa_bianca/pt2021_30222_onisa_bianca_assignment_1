package controller;

import model.Monom;
import model.Polinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtragereDate {
    public static void extrage() {

        Controller.getV().getFr().getConf1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String s1 = Controller.getV().getFr().getPolin1t().getText();
                    String[] s2 = s1.split("((?=\\+)|(?=\\-))");
                    Controller.getM().setP1(new Polinom());
                    for (String i : s2) {
                        String[] aux = i.split("\\^");
                        if (aux.length == 2) {
                            aux[0] = aux[0].replace("x", "");
                            if (aux[0].equals("")) {
                                Monom m = new Monom(1, Integer.parseInt(aux[1]));
                                Controller.getM().getP1().getPolin().add(m);
                            } else if (aux[0].equals("-")) {
                                Monom m = new Monom(-1, Integer.parseInt(aux[1]));
                                Controller.getM().getP1().getPolin().add(m);
                            } else {
                                Monom m = new Monom(Integer.parseInt(aux[0]), Integer.parseInt(aux[1]));
                                Controller.getM().getP1().getPolin().add(m);
                            }
                        } else if (aux.length == 1) {
                            if (aux[0].contains("x")) {
                                String[] aux2 = aux[0].split("x");
                                if(aux2.length==0)
                                {
                                    Monom m = new Monom(1, 1);
                                    Controller.getM().getP1().getPolin().add(m);
                                }
                                else if (aux2[0].equals("")) {
                                    Monom m = new Monom(1, 1);
                                    Controller.getM().getP1().getPolin().add(m);
                                } else if (aux2[0].equals("-")) {
                                    Monom m = new Monom(-1, 1);
                                    Controller.getM().getP1().getPolin().add(m);
                                } else {
                                    Monom m = new Monom(Integer.parseInt(aux2[0]), 1);
                                    Controller.getM().getP1().getPolin().add(m);
                                }
                            } else {
                                Monom m = new Monom(Integer.parseInt(aux[0]), 0);
                                Controller.getM().getP1().getPolin().add(m);
                            }

                        }
                    }
                }
                catch (Exception eq)
                {
                    Controller.getV().getE().getFrame1().setVisible(true);
                }
            }
        });

        Controller.getV().getFr().getConf2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String s1 = Controller.getV().getFr().getPolin2t().getText();
                    String[] s2 = s1.split("((?=\\+)|(?=\\-))");
                    Controller.getM().setP2(new Polinom());
                    for (String i : s2) {
                        String[] aux = i.split("\\^");
                        if (aux.length == 2) {
                            aux[0] = aux[0].replace("x", "");
                            if (aux[0].equals("")) {
                                Monom m = new Monom(1, Integer.parseInt(aux[1]));
                                Controller.getM().getP2().getPolin().add(m);
                            } else if (aux[0].equals("-")) {
                                Monom m = new Monom(-1, Integer.parseInt(aux[1]));
                                Controller.getM().getP2().getPolin().add(m);
                            } else {
                                Monom m = new Monom(Integer.parseInt(aux[0]), Integer.parseInt(aux[1]));
                                Controller.getM().getP2().getPolin().add(m);
                            }
                        } else if (aux.length == 1) {
                            if (aux[0].contains("x")) {
                                String[] aux2 = aux[0].split("x");
                                if(aux2.length==0)
                                {
                                    Monom m = new Monom(1, 1);
                                    Controller.getM().getP2().getPolin().add(m);
                                }
                                else if (aux2[0].equals("")) {
                                    Monom m = new Monom(1, 1);
                                    Controller.getM().getP2().getPolin().add(m);
                                } else if (aux2[0].equals("-")) {
                                    Monom m = new Monom(-1, 1);
                                    Controller.getM().getP2().getPolin().add(m);
                                } else {
                                    Monom m = new Monom(Integer.parseInt(aux2[0]), 1);
                                    Controller.getM().getP2().getPolin().add(m);
                                }
                            } else {
                                Monom m = new Monom(Integer.parseInt(aux[0]), 0);
                                Controller.getM().getP2().getPolin().add(m);
                            }

                        }
                    }
                }
                catch (Exception eq)
                {
                    Controller.getV().getE().getFrame1().setVisible(true);
                }
            }
        });
    }

}
