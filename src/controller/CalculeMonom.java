package controller;
import model.Monom;

import static java.lang.Math.abs;

public class CalculeMonom {

	public static Monom aduna(Monom m1, Monom m2)
    {
  	  Monom m3=new Monom(m1.getCoeficient()+m2.getCoeficient(),m1.getPutere());
  	  return m3;
    }
    
    public static Monom scade(Monom m1, Monom m2)
    {
  	  Monom m3=new Monom(m1.getCoeficient()-m2.getCoeficient(),m1.getPutere());
  	  return m3;
    }
    
    public static Monom inmult(Monom m1, Monom m2)
    {
  	  Monom m3=new Monom(m1.getCoeficient()*m2.getCoeficient(),m1.getPutere()+m2.getPutere());
  	  return m3;
    }

    public static Monom impart(Monom m1, Monom m2)
	{
		Monom m3=new Monom(0,0);
		if(m1.getCoeficient()/m2.getCoeficient()==0)
		{
			m3.setCoeficient(1);
			m3.setPutere(m1.getPutere()-m2.getPutere());
		}
		else
		{
			m3.setCoeficient(m1.getCoeficient()/m2.getCoeficient());
			m3.setPutere(m1.getPutere()-m2.getPutere());
		}
		return m3;
	}

    public static Monom deriv(Monom m1)
    {
  	  Monom m2=new Monom(0,0);
  	  if(m1.getPutere()>=1)
  	  {
  	      m2.setCoeficient(m1.getCoeficient()*m1.getPutere());
  	      m2.setPutere(m1.getPutere()-1);
  	  }
  	  else if(m1.getPutere()<1)
  	  {
  		  m2.setCoeficient(0);
  		  m2.setPutere(0);
  	  }
  	  return m2;
    }
    
    public static Monom integ(Monom m1)
    {
  	  Monom m2=new Monom(0,0);
  	  if(abs(m1.getCoeficient())<=abs((m1.getPutere()+1)))
  	  {
  		  m2.setCoeficient(1);
  		  m2.setPutere(m1.getPutere()+1);
  	  }
  	  else if(abs(m1.getCoeficient())>(m1.getPutere()+1))
  	  {
  		  m2.setCoeficient(m1.getCoeficient()/(m1.getPutere()+1));
  		  m2.setPutere(m1.getPutere()+1);
  	  }
  	  
  	  return m2;
    }


}
