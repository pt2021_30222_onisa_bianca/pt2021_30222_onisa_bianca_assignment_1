package view;

import javax.swing.*;
import java.awt.*;

public class Exceptie {

   private JFrame frame1;
   private JLabel l1;
   private JLabel ex;

    public Exceptie() {
        frame1 = new JFrame("Date Incorecte");
        frame1.setLayout(null);
        frame1.setBounds(600, 300, 450, 250);
        frame1.getContentPane().setBackground(new Color(234, 47, 51));

        l1= new JLabel("Date incorecte!");
        l1.setBounds(120,30,230,50);
        l1.setFont(new Font("Georgia",Font.BOLD, 25));
        frame1.add(l1);

        ex= new JLabel("Exemplu: 2x^3-6x^2+5x+4");
        ex.setBounds(80,100,280,50);
        ex.setFont(new Font("Georgia",Font.BOLD, 20));
        frame1.add(ex);
    }

    public JFrame getFrame1() {
        return frame1;
    }

    public void setFrame1(JFrame frame1) {
        this.frame1 = frame1;
    }
}
