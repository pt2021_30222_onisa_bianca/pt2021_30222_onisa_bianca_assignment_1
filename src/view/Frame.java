package view;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Frame {

	private JFrame frame1;
	private JLabel polin1;
	private  JTextField polin1t;
	private  JButton conf1;
	private JLabel polin2;
	private JTextField polin2t;
	private JButton conf2;
	private JButton adunare;
	private JButton scadere;
    private   JButton inmultire;
    private JButton impartire;
    private JButton integrare;
	private JButton derivare;
	private JLabel rezultat;
	private   JTextField rezT;
	private JLabel cat;
	private   JTextField catT;

	public Frame() {
		frame1=new JFrame("Calculator");
        frame1.setLayout(null);
	    frame1.setBounds(400,150,800,600);
	    frame1.getContentPane().setBackground(new Color(128, 255, 255));
	   
	    polin1= new JLabel("Dati primul polinom:");
	    polin1.setBounds(80,30,230,50);
	    polin1.setFont(new Font("Georgia",Font.BOLD, 20));
	    frame1.add(polin1);
	    
	    polin1t= new JTextField();
	    polin1t.setBounds(80,70,300,30);
	    frame1.add(polin1t);

	    conf1=new JButton("Confirma");
	    conf1.setBounds(500,70,150,30);
	    conf1.setBackground(new Color(77, 77, 255));
	    frame1.add(conf1);
	    
	    polin2= new JLabel("Dati al doilea polinom:");
	    polin2.setBounds(80,120,250,30);
	    polin2.setFont(new Font("Georgia",Font.BOLD, 20));
	    frame1.add(polin2);
	    
	    polin2t= new JTextField();
	    polin2t.setBounds(80,155,300,30);
	    frame1.add(polin2t);
	    
	    conf2=new JButton("Confirma");
	    conf2.setBounds(500,155,150,30);
	   conf2.setBackground(new Color(77, 77, 255));
	     frame1.add(conf2);

	     adunare=new JButton("Adunare");
	    adunare.setBounds(120,230,120,50);
  	    adunare.setBackground(new Color(191, 255, 128));
	    frame1.add(adunare);
	    
	   scadere=new JButton("Scadere");
	    scadere.setBounds(320,230,120,50);
	    scadere.setBackground(new Color(191, 255, 128));
	    frame1.add(scadere);

	    inmultire=new JButton("Inmultire");
	    inmultire.setBounds(520,230,120,50);
	    inmultire.setBackground(new Color(191, 255, 128));
	    frame1.add(inmultire);
	    
	    impartire=new JButton("Impartire");
	    impartire.setBounds(120,300,120,50);
	    impartire.setBackground(new Color(191, 255, 128));
	    frame1.add(impartire);
	    
	    integrare=new JButton("Integrare");
	    integrare.setBounds(320,300,120,50);
	    integrare.setBackground(new Color(191, 255, 128));
	    frame1.add(integrare);
	    
	     derivare=new JButton("Derivare");
	    derivare.setBounds(520,300,120,50);
	    derivare.setBackground(new Color(191, 255, 128));
	    frame1.add(derivare);
	    
	    rezultat= new JLabel("Rezultatul este:");
	    rezultat.setBounds(280,390,200,50);
	    rezultat.setFont(new Font("Georgia",Font.BOLD, 25));
	    frame1.add(rezultat);
	    
	    rezT= new JTextField();
	    rezT.setBounds(230,440,300,50);
	    frame1.add(rezT);

		cat= new JLabel("Restul impartirii este:");
		cat.setBounds(260,500,200,50);
		cat.setFont(new Font("Georgia",Font.BOLD, 15));
		frame1.add(cat);

		catT= new JTextField();
		catT.setBounds(450,500,200,40);
		frame1.add(catT);
	    
	    frame1.setVisible(true);
	}

	public JFrame getFrame1() {
		return frame1;
	}

	public void setFrame1(JFrame frame1) {
		this.frame1 = frame1;
	}

	public JLabel getPolin1() {
		return polin1;
	}

	public void setPolin1(JLabel polin1) {
		this.polin1 = polin1;
	}

	public JTextField getPolin1t() {
		return polin1t;
	}

	public void setPolin1t(JTextField polin1t) {
		this.polin1t = polin1t;
	}

	public JButton getConf1() {
		return conf1;
	}

	public void setConf1(JButton conf1) {
		this.conf1 = conf1;
	}

	public JLabel getPolin2() {
		return polin2;
	}

	public void setPolin2(JLabel polin2) {
		this.polin2 = polin2;
	}

	public JTextField getPolin2t() {
		return polin2t;
	}

	public void setPolin2t(JTextField polin2t) {
		this.polin2t = polin2t;
	}

	public JButton getConf2() {
		return conf2;
	}

	public void setConf2(JButton conf2) {
		this.conf2 = conf2;
	}

	public JButton getAdunare() {
		return adunare;
	}

	public void setAdunare(JButton adunare) {
		this.adunare = adunare;
	}

	public JButton getScadere() {
		return scadere;
	}

	public void setScadere(JButton scadere) {
		this.scadere = scadere;
	}

	public JButton getInmultire() {
		return inmultire;
	}

	public void setInmultire(JButton inmultire) {
		this.inmultire = inmultire;
	}

	public JButton getImpartire() {
		return impartire;
	}

	public void setImpartire(JButton impartire) {
		this.impartire = impartire;
	}

	public JButton getIntegrare() {
		return integrare;
	}

	public void setIntegrare(JButton integrare) {
		this.integrare = integrare;
	}

	public JButton getDerivare() {
		return derivare;
	}

	public void setDerivare(JButton derivare) {
		this.derivare = derivare;
	}

	public JLabel getRezultat() {
		return rezultat;
	}

	public void setRezultat(JLabel rezultat) {
		this.rezultat = rezultat;
	}

	public JTextField getRezT() {
		return rezT;
	}

	public void setRezT(JTextField rezT) {
		this.rezT = rezT;
	}

	public JLabel getCat() {
		return cat;
	}

	public void setCat(JLabel cat) {
		this.cat = cat;
	}

	public JTextField getCatT() {
		return catT;
	}

	public void setCatT(JTextField catT) {
		this.catT = catT;
	}
}
