package view;

public class View {
	
	private Frame fr;
	private Exceptie e;
	
	public View() {
     fr=new Frame();
     e=new Exceptie();
	}

	public Frame getFr() {
		return fr;
	}

	public void setFr(Frame fr) {
		this.fr = fr;
	}

	public Exceptie getE() {
		return e;
	}

	public void setE(Exceptie e) {
		this.e = e;
	}
}
